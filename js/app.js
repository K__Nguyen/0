/*
Wait for DOM content
*/
document.addEventListener('DOMContentLoaded', () =>
{
    /*
    # Mise en place d'un lecteur de support de cours
    L'exercice qui vous est propos� vas vous permettre de r�aliser une WebApp qui affiche les supports de cours qui vous on �t� fourni lors de vos sessions. Les principes � mettre en place sont exactement les m�mes que ceux vue en cours, aucun pi�ge dans cette exercice, il vous suffi d'�tre capable de r�p�ter ce que vous avez vu en cours dans un cadre l�g�rement diff�rent. Et en vous proposant de d�couvrir plus en profondeur le format Markdown.

    Ce format est celui utilis� pour vos supports de cours, mais aussi pour les fichiers README que vous devez cr�er pour chacun de vos r�petoires. Le principe de ce format est qu'il est facile a convertir dans d'autres formats, par exemple en HTML. Ce que vous allez devoir faire dans cet exercice et d'�x�cuter une requ�te vers des fichier Markdown, pour les afficher en HTML dans votre page. Pour ce faire, vous avez pour commencer toutes les techniques vues en cours, puis le module Marked (https://www.npmjs.com/package/marked), qui est d�ja charg� dans le fichier 'index.html'. Afficher la page web dans votre navigateur et inspect� votre console, l'int�ret du format Markdown est la simplicit� avec laquelle on peut le convertir.

    Prennez le temps de bien lire l'ennon� suivant, et bon courage!

    ## Liste des t�ches :
    - Capter l'�v�nement clic sur chaque bouton de la section 'section-courses-list'
    - Une fois l'�v�nement capt�, r�cup�rer la valeur de la propri�t� 'data-link'
    - Utiliser la valeur de 'data-link' pour ex�cuter une requ�te HTTP afin de r�cup�rer le contenu d'un support de cours
    - Une fois le contenu r�cup�r�, cr�er une section dans le DOM pour l'afficher avec Marked
    */



    // Exemple d'utilisation du module Marked
    let newDiv = document.createElement("div");
    let main = document.querySelector('main');
    main.append(newDiv);

    var x = document.getElementById('section-courses-list');
    x.addEventListener("click", (event) =>
    {
        const isButton = event.target.nodeName === 'BUTTON';
        if (!isButton)
        {
            console.log('no');
            return;
        }
        const fetchRequest = new FetchRequest(
            `${event.target.dataset.link}`,
            'text'
        );
        fetchRequest.init();
        fetchRequest.sendRequest()
            .then(
                response => newDiv.innerHTML = marked.parse(response)
            );
    })
})
